# Goliath-models

This git repository contains 3 cell-specific models in SBML format which were generated for the GOLIATH project.

## Repository summary

This repository contains the following 4 models:

```
GOLIATH_adipocyte_model.xml
GOLIATH_hepatocyte_model.xml
GOLIATH_pancreas_model.xml
human_model.xml
```

The first 3 models are cell-specific models of adipoctes, hepatocytes, and pancreatic beta-cells respectively.  
The 4th file is the starting model which was used for generating the 3 cell-specific models.  
Below are some summary statistics of the 4 models.  


| networks    | adipocyte | hepatocyte  | pancreas  | human-model |
|-------------|----------:|------------:|----------:|------------:|
| reactions   | 11150     | 11306       | 10881     | 11719       |
| metabolites | 6763      | 6869        | 6640      | 7012        |
| genes       | 2407      | 2409        | 2383      | 2430        | 

## Method overview

The starting model is based on Human-GEM version 1.14.  
The bounds of the biomass maintenance reaction MAR09931 have been set to (0, 1000), and the bounds of the generic human biomass reaction MAR13082 have been set to (0, 0).  
Subsequently, reactions which cannot carry flux have been removed. This is the model which is present in this repository.  
The cell-specific models were generated using OCMMED: https://forgemia.inra.fr/metexplore/cbm/ocmmed  
For generating the cell-specific models, the biomass maintenance reaction MAR09931 was forced to carry flux with a lower bound of 0.01 mmol/g/hr.  

